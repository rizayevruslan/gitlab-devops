@servers(['web' => 'root@vps.firstcoders.store'])

@task('list', ['on' => 'web'])
    ls -l
@endtask

@task('newfolder', ['on' => 'web'])
    cd /web/test.first-coders.ru/public_html/devops6/
    git stash
    git pull 
@endtask
